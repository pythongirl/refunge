use spade::{rtree::RTree, SpatialObject, BoundingRect};
use crate::state::{Pos, FungeSpace, FungeStack};
use std::convert::TryFrom;

enum FlowTerminator {
    Redirector(Box<dyn Fn(&mut FungeStack, Pos) -> Pos>),
    Put
}


fn reflect(_stack: &mut FungeStack, direction: Pos) -> Pos {
    direction * -1
}

struct Flow {
    direction: Pos,
    position: Pos,
    end_position: Pos,

    instructions: Vec<Box<dyn Fn(&mut FungeStack, &FungeSpace)>>,
    terminator: FlowTerminator,
}

impl SpatialObject for Flow {
    type Point = Pos;

    fn mbr(&self) -> BoundingRect<Pos> {
        BoundingRect::from_corners(&self.position, &self.end_position) 
    }

    fn distance2(&self, other: &Pos) -> i64 {
        let pos = self.position;

        (pos.0 - other.0).pow(2) + (pos.1 - other.1).pow(2)
    }
}

impl Flow {
    fn construct(space: &FungeSpace, position: Pos, direction: Pos) -> Flow {
        let mut ip = position;
        let mut instructions: Vec<Box<dyn Fn(&mut FungeStack, &FungeSpace)>> = Vec::new();

        let terminator = loop {
            if !space.mbr().contains_point(ip) {
                
            }

            let cell = space.get(ip);

            let code_point = match u32::try_from(cell) {
                Ok(v) => v,
                _ => break FlowTerminator::Redirector(Box::new(reflect))
            };

            let instruction = match char::try_from(code_point) {
                Ok(v) => v,
                _ => break FlowTerminator::Redirector(Box::new(reflect))
            };

            instructions.push(match instruction {
                '0'...'9' | 'a'...'f' => Box::new(move |stack: &mut FungeStack, _space| {
                    stack.push(instruction.to_digit(16).unwrap().into())
                }),
                'g' => Box::new(move |stack: &mut FungeStack, space: &FungeSpace| {
                    let (y, x) = stack.pop2();
                    stack.push(space.get(Pos(x, y)))
                }),
                'p' => {
                    break FlowTerminator::Put;
                },
                'r' | _ => {
                    break FlowTerminator::Redirector(Box::new(reflect));
                }
            });

            ip += direction;
        };

        Flow {
            direction,
            position,
            end_position: ip,
            instructions,
            terminator
        }
    }
}

// enum Instruction {
//     Continuation(char),
//     Termination(char)
// }

// enum ContinuationInstruction {
//     Digit(char),
//     Add,
//     Subtract,
//     Multiply,
//     Divide,
//     Modulo,
//     Not,
//     GreaterThan,
//     Duplicate,
//     Swap,
//     Pop,
//     WriteInteger,
//     WriteCharacter,
//     Get,
//     ReadInteger,
//     ReadChar,
//     String,
//     Quote,
// }

// enum TerminationInstruction {
//     Put,
//     Up,
//     Down,
//     Left,
//     Right,
//     Random,
//     HorizonalIf,
//     VerticalIf,
//     Reflect,

//     Terminate,
// }
