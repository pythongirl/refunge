use std::convert::TryFrom;
use std::fs;
use std::io::{self, Read, Write};

use rand::prelude::*;

mod state;
use state::{FungeSpace, FungeStack, Pos, directions::*};

mod instructions;

mod flows;

struct FungeState {
    // Registers
    ip: Pos,
    delta: Pos,

    // State
    space: FungeSpace,
    stack: FungeStack,

    string_mode: bool,
    quote_mode: bool,

    input: Box<dyn Read>,
    output: Box<dyn Write>,
}

// Example: pure_stack_op!(self.stack; b, a; a * b, a + b)
macro_rules! pure_stack_op {
    ($stack:expr ; $($name:ident),* ; $($value:expr),* ) => {{
        $( let $name = $stack.pop(); )*
        $( $stack.push($value); )*
    }};
}

// Example: effect_stack_op!(self.stack; a, b; {
//     state_var = a + b;
// })
macro_rules! effect_stack_op {
    ($stack:expr ; $($name:ident),* ; { $($e:tt)* } ) => {{
        $( let $name = $stack.pop(); )*
        { $($e)* }
    }};
}

impl FungeState {
    fn new(data: String) -> FungeState {
        FungeState {
            space: FungeSpace::new(data),
            ip: Pos(0, 0),
            delta: RIGHT,
            stack: FungeStack::new(),

            string_mode: false,
            quote_mode: false,
            input: Box::new(io::stdin()),
            output: Box::new(io::stdout()),
        }
    }

    fn advance(&mut self) -> Result<(), &'static str> {
        let instruction = self.space.get(self.ip);

        if self.string_mode {
            if instruction == '"' as i64 {
                self.string_mode = false;
            } else {
                self.stack.push(instruction);
            }
        } else if self.quote_mode {
            self.stack.push(instruction);
            self.quote_mode = false;
        } else {
            match u32::try_from(instruction).map(char::try_from) {
                Ok(Ok(char_instruction)) => {
                    self.execute_instruction(char_instruction)?;
                }
                _ => {
                    unimplemented!();
                }
            }
        }

        self.increment_ip();

        Ok(())
    }

    fn increment_ip(&mut self) {
        self.ip += self.delta;
    }

    fn execute_instruction(&mut self, instruction: char) -> Result<(), &'static str> {
        // dbg!(&self.stack.0);
        // dbg!(instruction);

        match instruction {
            '0'...'9' => pure_stack_op!(self.stack; ; i64::from(instruction.to_digit(10).unwrap())),
            ' ' => { /* pass */ }
            '+' => pure_stack_op!(self.stack; b, a; a + b),
            '-' => pure_stack_op!(self.stack; b, a; a - b),
            '*' => pure_stack_op!(self.stack; b, a; a * b),
            '/' => pure_stack_op!(self.stack; b, a; a / b),
            '%' => pure_stack_op!(self.stack; b, a; a % b),
            '!' => pure_stack_op!(self.stack; a; if a == 0 { 1 } else { 0 }),
            '`' => pure_stack_op!(self.stack; b, a; if a > b { 1 } else { 0 }),

            '>' | '<' | '^' | 'v' => {
                self.delta = match instruction {
                    '>' => RIGHT,
                    '<' => LEFT,
                    '^' => UP,
                    'v' => DOWN,
                    _ => unreachable!(),
                }
            }

            '?' => {
                self.delta = match random::<i32>() % 4 {
                    0 => UP,
                    1 => DOWN,
                    2 => LEFT,
                    3 => RIGHT,
                    _ => unreachable!(),
                };
            }

            '_' => effect_stack_op!(self.stack; a; {
                self.delta = if a != 0 { LEFT } else { RIGHT };
            }),
            '|' => effect_stack_op!(self.stack; a; {
                self.delta = if a != 0 { UP } else { DOWN };
            }),
            '"' => {
                self.string_mode = true;
            }
            ':' => pure_stack_op!(self.stack; a; a, a),
            '\\' => pure_stack_op!(self.stack; b, a; b, a),
            '$' => pure_stack_op!(self.stack; _deleted; ),
            '.' => effect_stack_op!(self.stack; a; {
                write!(self.output, "{} ", a).map_err(|_| "Unable to write integer")?;
            }),
            ',' => effect_stack_op!(self.stack; a; {
                let c = char::try_from(u32::try_from(a).unwrap()).unwrap();
                write!(self.output, "{} ", c).map_err(|_| "Unable to write char")?;
            }),
            '#' => {
                self.increment_ip();
            }
            'g' => pure_stack_op!(self.stack; y, x; self.space.get(Pos(x, y))),
            'p' => effect_stack_op!(self.stack; y, x, value; {
                self.space.set(Pos(x, y), value);
            }),
            '&' => {
                unimplemented!();
            }
            '~' => {
                let mut value = [0u8; 1];
                self.input
                    .read_exact(&mut value)
                    .map_err(|_| "Unable to read from stdin")?;
                self.stack.push(i64::from(value[0]));
            }
            '@' => {
                self.delta = Pos(0, 0);
            }

            // Extensions
            'a'...'f' => {
                self.stack
                    .push(i64::from(instruction.to_digit(16).unwrap()));
            }
            '\'' => {
                self.quote_mode = true;
            }

            _ => {
                unimplemented!();
            }
        };

        Ok(())
    }
}

fn main() {
    let input_file = fs::read_to_string("./test.b93").expect("Unabel to read file");
    let mut state = FungeState::new(input_file);

    while state.delta != Pos(0, 0) {
        state.advance().unwrap();
    }
}
