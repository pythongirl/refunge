use std::ops::{Add, AddAssign, Mul};

use spade::{rtree::RTree, BoundingRect, PointN, SpatialObject, TwoDimensional};

#[derive(Clone, Copy, Debug, PartialEq, Eq)]
pub struct Pos(pub i64, pub i64);

pub mod directions {
    use super::Pos;

    pub const UP: Pos = Pos(0, -1);
    pub const DOWN: Pos = Pos(0, 1);
    pub const LEFT: Pos = Pos(-1, 0);
    pub const RIGHT: Pos = Pos(1, 0);
}

impl Add for Pos {
    type Output = Pos;
    fn add(self, other: Pos) -> Pos {
        Pos(self.0 + other.0, self.1 + other.1)
    }
}

impl AddAssign for Pos {
    fn add_assign(&mut self, other: Pos) {
        *self = *self + other;
    }
}

impl Mul<i64> for Pos {
    type Output = Pos;
    
    fn mul(self, other: i64) -> Pos {
        Pos(self.0 * other, self.1 * other)
    }
}

impl PointN for Pos {
    type Scalar = i64;

    fn dimensions() -> usize {
        2
    }

    fn from_value(value: i64) -> Pos {
        Pos(value, value)
    }

    fn nth(&self, index: usize) -> &i64 {
        match index {
            0 => &self.0,
            1 => &self.1,
            _ => unreachable!(),
        }
    }

    fn nth_mut(&mut self, index: usize) -> &mut i64 {
        match index {
            0 => &mut self.0,
            1 => &mut self.1,
            _ => unreachable!(),
        }
    }
}

impl TwoDimensional for Pos {}

#[derive(Clone)]
struct FungeCell {
    value: i64,
    position: Pos,
}

impl FungeCell {
    fn new(value: i64, position: Pos) -> FungeCell {
        FungeCell { value, position }
    }
}

impl SpatialObject for FungeCell {
    type Point = Pos;

    fn mbr(&self) -> BoundingRect<Pos> {
        BoundingRect::from_point(self.position)
    }

    fn distance2(&self, other: &Pos) -> i64 {
        let pos = self.position;

        (pos.0 - other.0).pow(2) + (pos.1 - other.1).pow(2)
    }
}

pub struct FungeSpace {
    inner: RTree<FungeCell>,
}

impl FungeSpace {
    pub fn new(input: String) -> FungeSpace {
        let data = input
            .lines()
            .enumerate()
            .flat_map(|(y, line)| {
                line.chars()
                    .enumerate()
                    .map(move |(x, c)| FungeCell::new(c as i64, Pos(x as i64, y as i64)))
            })
            .filter(|c| c.value != 32)
            .collect::<Vec<_>>();

        FungeSpace {
            inner: RTree::bulk_load(data),
        }
    }

    pub fn get(&self, pos: Pos) -> i64 {
        self.inner.lookup(&pos).map(|cell| cell.value).unwrap_or(32)
    }

    pub fn set(&mut self, pos: Pos, value: i64) {
        self.inner.lookup_and_remove(&pos);

        self.inner.insert(FungeCell::new(value, pos));
    }
}


#[repr(transparent)]
#[derive(Debug)]
pub struct FungeStack(Vec<i64>);

impl FungeStack {
    pub fn new() -> FungeStack {
        FungeStack(Vec::new())
    }

    pub fn push(&mut self, value: i64) {
        self.0.push(value)
    }

    pub fn pop(&mut self) -> i64 {
        self.0.pop().unwrap_or(0)
    }

    pub fn pop2(&mut self) -> (i64, i64) {
        let v1 = self.pop();
        let v2 = self.pop();

        (v1, v2)
    }

    pub fn pop3(&mut self) -> (i64, i64, i64) {
        let v1 = self.pop();
        let v2 = self.pop();
        let v3 = self.pop();
        
        (v1, v2, v3)
    }
}